$(document).ready(function() {
	$('body').addClass('loaded');

	//Swiper
	(function() {
		var galleryTop = new Swiper('#slider-bg', {
			direction: 'vertical',
			loop: true,
			speed: 1200,
			slidesPerView: 1,
			effect: 'fade',
			pagination: {
				el: '.top-swiper-pagination',
				clickable: true,
			},
			allowTouchMove: false,
			paginationClickable: true
		});
		var galleryThumbs = new Swiper('#slider-info', {
			direction: 'vertical',
			loop: true,
			speed: 1200,

			slidesPerView: 1,
			spaceBetween: 100,
			allowTouchMove: false,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.slider__info-pagination',
				type: 'fraction',
			},
			renderFraction: function (currentClass, totalClass) {
				return '<span class="' + currentClass + '"></span>' +
					'<span class="' + totalClass + '"></span>';
			}
		});
		galleryTop.controller.control = galleryThumbs;
		galleryThumbs.controller.control = galleryTop;
	})();
});
